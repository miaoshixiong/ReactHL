import { defineConfig } from 'vite'
import { resolve } from "path";
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
			alias: {
				"@": resolve(__dirname, "./src")
			}
		},
	css: {
			preprocessorOptions: {
				less: {
					// modifyVars: {
					// 	"primary-color": "#1DA57A",
					// },
					javascriptEnabled: true,
					additionalData: `@import "@/styles/var.less";`
				}
			}
		},
	build: {//打包配置
    chunkSizeWarningLimit:3000,//包的大小限制
    // rollupOptions: {//拆包配置
    //     output:{
    //         manualChunks(id) {
    //           if (id.includes('node_modules')) {
    //               return id.toString().split('node_modules/')[1].split('/')[0].toString();
    //           }
    //       }
    //     }
    // }
	}
})
