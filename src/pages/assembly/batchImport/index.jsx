import React, { useState } from "react";
import { Table } from "antd";
import UploadExcelComponent from "@/components/UploadExcel";

const UploadExcel = () => {
	const [tableData,settableData] = useState([])
	const [tableHeader,settableHeader] = useState([])
	const handleSuccess = ({ results, header })=>{
		settableData(results)
		settableHeader(header.map((item)=>(
			{title: item,dataIndex: item,key: item,}
			)))
		console.log(tableData,tableHeader)
	}

	return (
		<div className="app-container">
        <UploadExcelComponent uploadSuccess={handleSuccess} />
        <br />
        <Table dataSource={tableData} columns={tableHeader} />;
      </div>
	);
};

export default UploadExcel;
