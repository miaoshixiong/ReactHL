import React, { useState,useEffect } from "react";
import HLPdfPreview from '@/components/HLPdfPreview'

const pdfview = () => {
	const [url,seturl] = useState('')

	useEffect(()=>{
		seturl('http://218.93.142.22:8999//file/20220510/2022051014222808459637a1582-0a2b-4c30-a7f5-285de5f84488.pdf')
	},[])

	return (
		<div className="App">
      <HLPdfPreview prfUrl={url} />
    </div>
	);
};

export default pdfview;
