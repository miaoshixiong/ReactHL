import { useEffect } from "react";
import { Table, DatePicker, Button, Space } from "antd";
import useAuthButtons from "@/hooks/useAuthButtons";

import "./index.less";

const UseHooks = () => {
	// 按钮权限
	const { BUTTONS } = useAuthButtons();
	const { RangePicker } = DatePicker;

	const handleDownload = (type)=>{
			console.log('导出')
			import("@/utils/Export2Excel").then((excel) => {
			console.log(excel)
      const tHeader = columns.map(v=>v.title);
      const filterVal = columns.map(v=>v.dataIndex);
      // const list = type === "all" ? this.state.list : this.state.selectedRows;
			const list = dataSource
      const data = formatJson(filterVal, list);
      excel.export_json_to_excel({
        header: tHeader,
        data,
        filename: '111',
        autoWidth: true,
        bookType: 'xlsx',
      });
      // this.setState({
      //   selectedRowKeys: [], // 导出完成后将多选框清空
      //   downloadLoading: false,
      // });
    });
	}
	function formatJson(filterVal, jsonData) {
    return jsonData.map(v => filterVal.map(j => v[j]))
  }

	useEffect(() => {
		console.log(BUTTONS);
	}, []);

	const dataSource = [
		{
			key: "1",
			name: "胡彦斌",
			age: 32,
			address: "西湖区湖底公园1号"
		},
		{
			key: "2",
			name: "胡彦祖",
			age: 42,
			address: "西湖区湖底公园1号"
		},
		{
			key: "3",
			name: "刘彦祖",
			age: 18,
			address: "西湖区湖底公园1号"
		},
		{
			key: "4",
			name: "刘彦祖",
			age: 18,
			address: "翻斗大街翻斗花园二号楼1001室"
		},
		{
			key: "5",
			name: "刘彦祖",
			age: 18,
			address: "翻斗大街翻斗花园二号楼1001室"
		}
	];

	const columns = [
		{
			title: "姓名",
			dataIndex: "name",
			key: "name",
			align: "center"
		},
		{
			title: "年龄",
			dataIndex: "age",
			key: "age",
			align: "center"
		},
		{
			title: "住址",
			dataIndex: "address",
			key: "address",
			align: "center",
			width: "50%"
		}
	];
	return (
		<>
			<div className="date">
				<span>切换国际化的时候看我 😎 ：</span>
				<RangePicker />
			</div>
			<div className="auth">
				<Space>
					{<Button type="primary" onClick={()=>{handleDownload()}}>全部导出</Button>}
				</Space>
			</div>
			<Table dataSource={dataSource} columns={columns} />
		</>
	);
};

export default UseHooks;
