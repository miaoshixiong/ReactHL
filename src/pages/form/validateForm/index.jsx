import React,{useState,useEffect} from 'react'
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Row, Select, Card, Modal, Radio ,message,Space,Switch,Table,Popconfirm  } from 'antd';
const { Option } = Select;

//搜索块
const AdvancedSearchForm = ({ setOpen,setLoading}) => {

  const [expand, setExpand] = useState(false);//更多项的状态
  const [form] = Form.useForm();//定义表单

  //搜索条件选项
  const getFields = () => {
    const children = [];
    children.push(
      <Col span={6}  key={1}>
          <Form.Item label={'姓名'}
            name='nickname'
            rules={[]}>
            <Input />
          </Form.Item>
      </Col>,

    )
    if(expand){
      children.push(
        <Col span={6} key={2}>
          <Form.Item initialValue={'1'}
            name='sex'
            label={'性别'}
            // rules={[
            //   {
            //     required: true,
            //     message: 'Input something!',
            //   },
            // ]}
          >
            <Select>
                 <Option value="1">男</Option>
                 <Option value="2">女</Option>
            </Select>
          </Form.Item>
        </Col>,
      );
    }
    return children;
  };

  //提交事件
  const onFinish = (values) => {
    console.log('values of form: ', values);
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
    }, 2000);
    // //设置表单的值
    // form.setFieldsValue({
    //   nickname: 'Hello world!',
    // });
  };
  return (
    <Card>
    <Form
      form={form}
      // layout={'inline'}
      name="advanced_search"
      className="ant-advanced-search-form"
      onFinish={onFinish}
    >
      <Row gutter={20}>
        <Button type="primary"
          onClick={() => {
              setOpen(true);
            }}
        >+ 新增</Button>
        {getFields()}
        <Col
          span={6}
          style={{
            textAlign: 'right',
            marginLeft:'auto'
          }}
        >
          <Button type="primary" htmlType="submit">
            查询
          </Button>
          <Button
            style={{
              margin: '0 8px',
            }}
            onClick={() => {
              form.resetFields();
            }}
          >
            重置
          </Button>
          <a
            style={{
              fontSize: 12,
            }}
            onClick={() => {
              setExpand(!expand);
            }}
          >
            {expand ? <UpOutlined /> : <DownOutlined />} 更多项
          </a>
        </Col>
      </Row>
      {/* <Row>
        
      </Row> */}
    </Form>
    </Card>
  );
};

//表格
const TableDOM = ({ setOpen,seteditForm,loading,setLoading }) => {

const [data,setdata] = useState([]);//假数据
const mockdata = []
for (let i = 1; i <= 60; i++) {
  mockdata.push({
    key: i,
    name: 'John Brown',
    age: Number(`${i}2`),
    address: `New York No. ${i} Lake ParkLake Park`,
    description: `My name is John Brown, I am ${i}2 years old, living in New York No. ${i} `,
  });
}

  useEffect(()=>{
    setdata(mockdata)
  },[])

  const defaultTitle = () => 'Here is title';
  const defaultFooter = () => 'Here is footer';
  const defaultExpandable = {
    expandedRowRender: (record) => <p>{record.description}</p>,
  };

  const [bordered, setBordered] = useState(false);
  // const [loading, setLoading] = useState(false);
  const [size, setSize] = useState('large');
  const [expandable, setExpandable] = useState(defaultExpandable);
  const [showTitle, setShowTitle] = useState(false);
  const [showHeader, setShowHeader] = useState(true);
  const [showfooter, setShowFooter] = useState(true);
  const [rowSelection, setRowSelection] = useState({});
  const [hasData, setHasData] = useState(true);
  const [tableLayout, setTableLayout] = useState(undefined);
  const [top, setTop] = useState('none');
  const [bottom, setBottom] = useState('bottomRight');
  const [ellipsis, setEllipsis] = useState(false);
  const [yScroll, setYScroll] = useState(false);
  const [xScroll, setXScroll] = useState(undefined);

//删除事件
const handleDelete = (key) => {
    let newData = data.filter((item) => item.key !== key);
    setLoading(true)
    return new Promise((resolve) => {
      setTimeout(() => {
        setdata(newData);
        setLoading(false)
        message.success('删除成功')
        resolve(null)
      }, 2000);
    });
  };
const columns = [
  {
    title: '姓名',
    dataIndex: 'name',
  },
  {
    title: '年龄',
    dataIndex: 'age',
    sorter: (a, b) => a.age - b.age,
  },
  {
    title: '地址',
    dataIndex: 'address',
    filters: [
      {
        text: 'London',
        value: 'London',
      },
      {
        text: 'New York',
        value: 'New York',
      },
    ],
    onFilter: (value, record) => record.address.indexOf(value) === 0,
  },
  {
    title: '操作',
    key: 'action',
    sorter: true,
    render: (_, record) => (
      <Space size="middle">
        <Space onClick={(e) => {
              // console.log(record)
              setOpen(true);//打开弹窗
              seteditForm(record)//设置编辑行数据
            }}>
           <a>编辑</a>
        </Space>
        <Popconfirm title="确认删除?" onConfirm={() => handleDelete(record.key)}>
            <a>删除</a>
          </Popconfirm>
        <a>
          <Space>
            更多操作
            <DownOutlined />
          </Space>
        </a>
      </Space>
    ),
  },
];

  const handleBorderChange = (enable) => {
    setBordered(enable);
  };

  const handleLoadingChange = (enable) => {
    setLoading(enable);
  };

  const handleSizeChange = (e) => {
    setSize(e.target.value);
  };

  const handleTableLayoutChange = (e) => {
    setTableLayout(e.target.value);
  };

  const handleExpandChange = (enable) => {
    setExpandable(enable ? defaultExpandable : undefined);
  };

  const handleEllipsisChange = (enable) => {
    setEllipsis(enable);
  };

  const handleTitleChange = (enable) => {
    setShowTitle(enable);
  };

  const handleHeaderChange = (enable) => {
    setShowHeader(enable);
  };

  const handleFooterChange = (enable) => {
    setShowFooter(enable);
  };

  const handleRowSelectionChange = (enable) => {
    setRowSelection(enable ? {} : undefined);
  };

  const handleYScrollChange = (enable) => {
    setYScroll(enable);
  };

  const handleXScrollChange = (e) => {
    setXScroll(e.target.value);
  };

  const handleDataChange = (newHasData) => {
    setHasData(newHasData);
  };

  const scroll = {};

  if (yScroll) {
    scroll.y = 240;
  }

  if (xScroll) {
    scroll.x = '100vw';
  }

  const tableColumns = columns.map((item) => ({ ...item, ellipsis }));//ellipsis 超过宽度将自动省略 true时表格布局将变成 tableLayout="fixed"

  if (xScroll === 'fixed') {
    tableColumns[0].fixed = true;
    tableColumns[tableColumns.length - 1].fixed = 'right';
  }

  const tableProps = {
    bordered,//边框
    loading,//加载
    size,//Large大 Middle中 Small小
    expandable,//配置展开属性
    title: showTitle ? defaultTitle : undefined,//上标题
    showHeader,//表头
    footer: showfooter ? defaultFooter : undefined,//下标题
    rowSelection,//行复选框
    scroll,//宽高
    tableLayout,//设为 fixed 表示内容不会影响列的布局
  };
  return (
    <>
      <Form
        layout="inline"
        className="components-table-demo-control-bar"
        style={{
          marginBottom: 16,
        }}
      >
        <Form.Item label="Bordered">
          <Switch checked={bordered} onChange={handleBorderChange} />
        </Form.Item>
        <Form.Item label="loading">
          <Switch checked={loading} onChange={handleLoadingChange} />
        </Form.Item>
        <Form.Item label="Title">
          <Switch checked={showTitle} onChange={handleTitleChange} />
        </Form.Item>
        <Form.Item label="Column Header">
          <Switch checked={showHeader} onChange={handleHeaderChange} />
        </Form.Item>
        <Form.Item label="Footer">
          <Switch checked={showfooter} onChange={handleFooterChange} />
        </Form.Item>
        <Form.Item label="Expandable">
          <Switch checked={!!expandable} onChange={handleExpandChange} />
        </Form.Item>
        <Form.Item label="Checkbox">
          <Switch checked={!!rowSelection} onChange={handleRowSelectionChange} />
        </Form.Item>
        <Form.Item label="Fixed Header">
          <Switch checked={!!yScroll} onChange={handleYScrollChange} />
        </Form.Item>
        <Form.Item label="Has Data">
          <Switch checked={!!hasData} onChange={handleDataChange} />
        </Form.Item>
        <Form.Item label="Ellipsis">
          <Switch checked={!!ellipsis} onChange={handleEllipsisChange} />
        </Form.Item>
        <Form.Item label="Size">
          <Radio.Group value={size} onChange={handleSizeChange}>
            <Radio.Button value="large">Large</Radio.Button>
            <Radio.Button value="middle">Middle</Radio.Button>
            <Radio.Button value="small">Small</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Table Scroll">
          <Radio.Group value={xScroll} onChange={handleXScrollChange}>
            <Radio.Button value={undefined}>Unset</Radio.Button>
            <Radio.Button value="scroll">Scroll</Radio.Button>
            <Radio.Button value="fixed">Fixed Columns</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Table Layout">
          <Radio.Group value={tableLayout} onChange={handleTableLayoutChange}>
            <Radio.Button value={undefined}>Unset</Radio.Button>
            <Radio.Button value="fixed">Fixed</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Pagination Top">
          <Radio.Group
            value={top}
            onChange={(e) => {
              setTop(e.target.value);
            }}
          >
            <Radio.Button value="topLeft">TopLeft</Radio.Button>
            <Radio.Button value="topCenter">TopCenter</Radio.Button>
            <Radio.Button value="topRight">TopRight</Radio.Button>
            <Radio.Button value="none">None</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Pagination Bottom">
          <Radio.Group
            value={bottom}
            onChange={(e) => {
              setBottom(e.target.value);
            }}
          >
            <Radio.Button value="bottomLeft">BottomLeft</Radio.Button>
            <Radio.Button value="bottomCenter">BottomCenter</Radio.Button>
            <Radio.Button value="bottomRight">BottomRight</Radio.Button>
            <Radio.Button value="none">None</Radio.Button>
          </Radio.Group>
        </Form.Item>
      </Form>
      <Table
        {...tableProps}//配置项
        pagination={{
          position: [top, bottom],
        }}
        columns={tableColumns}//列头
        dataSource={hasData ? data : []}//表格数据
        scroll={scroll}//
      />
    </>
  );
};

//新增编辑弹窗
const CollectionCreateForm = ({ open, onOk, onCancel,confirmLoading,editForm}) => {
  const [form] = Form.useForm();
  if(editForm){
    console.log('编辑数据',editForm)
    //设置表单的值
    form.setFieldsValue(editForm);
  }else{
     form.setFieldsValue({});
  }
  return (
    <Modal
      getContainer={false}
      open={open}
      title="新增"
      okText="确认"
      cancelText="取消"
      confirmLoading={confirmLoading}
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()//表单验证
          .then((values) => {//成功
            form.resetFields();
            onOk(values);
          })
          .catch((info) => {//失败
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: 'Please input the title of collection!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input type="textarea" />
        </Form.Item>
        <Form.Item name="modifier" className="collection-create-form_last-form-item">
          <Radio.Group>
            <Radio value="public">Public</Radio>
            <Radio value="private">Private</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
};

const App = () => {
  const [open, setOpen] = useState(false);//定义弹窗开关
  const [confirmLoading, setConfirmLoading] = useState(false);//定义加载开关
  const [editForm, seteditForm] = useState({});//定义编辑行数据
  const [loading, setLoading] = useState(false);//表格加载
  const onOk = () => {//新增确认
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
      message.success('新增成功')
    }, 2000);
  };
  return (
    <div>
      {/* 搜索栏 */}
      <AdvancedSearchForm setOpen={setOpen} setConfirmLoading={setConfirmLoading} setLoading={setLoading} />

      {/* 表格 */}
      <TableDOM setOpen={setOpen} seteditForm={seteditForm} loading={loading} setLoading={setLoading} />

      {/* 弹窗组件 */}
      <CollectionCreateForm
        editForm={editForm}
        open={open}//开关
        onOk={onOk}//确定
        confirmLoading={confirmLoading}//加载动画
        onCancel={() => {
          setOpen(false);//关闭
        }}
      />
    </div>
  );
}

export default App;
