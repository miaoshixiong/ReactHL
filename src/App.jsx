import { useState } from 'react'
import { HashRouter } from "react-router-dom";
import { ConfigProvider } from "antd";
import Router from "@/routers/index";
import AuthRouter from "@/routers/utils/authRouter";
import { useSelector } from "@/redux";
import useTheme from "@/hooks/useTheme";

function App() {
	const { assemblySize,themeConfig } = useSelector((state) => state.global);

	console.log('主题设置参数',themeConfig)
	// 全局使用主题
	useTheme(themeConfig)

  return (
  <HashRouter>
		<ConfigProvider componentSize={assemblySize}>
			<AuthRouter>
				<Router />
			</AuthRouter>
		</ConfigProvider>
	</HashRouter>
  )
}

export default App
