import { createSlice } from "@reduxjs/toolkit";

const globalState = {
	token: "",
	userInfo: "",
	assemblySize: "middle",
	language: "",
	themeConfig: {
		// 默认 primary 主题颜色
		primary: "#1890ff",
		// 深色模式
		isDark: true,
		// 色弱模式(weak) || 灰色模式(gray)
		weakOrGray: "",
		// 面包屑导航
		breadcrumb: true,
		// 标签页
		tabs: true,
		// 页脚
		footer: true
	}
};

const globalSlice = createSlice({
	name: "global",
	initialState: globalState,
	reducers: {
		setToken(state, { payload }) {
			state.token = payload;
		},
		setAssemblySize(state, { payload }) {
			state.assemblySize = payload;
		},
		setLanguage(state, { payload }) {
			state.language = payload;
		},
		setDark(state, { payload }) {
			state.themeConfig.isDark = payload;
		},
		setWeakOrGray(state, { payload }) {
			state.themeConfig.weakOrGray = payload;
		},
		setThemeConfig(state, { payload }) {
			state.themeConfig = payload;
		}
	}
});

export const { setToken, setAssemblySize, setLanguage, setDark, setWeakOrGray,setThemeConfig } = globalSlice.actions;
export default globalSlice.reducer;
