import { createSlice } from "@reduxjs/toolkit";

const breadcrumbState = {
	breadcrumbList: {}
};

const breadcrumbSlice = createSlice({
	name: "breadcrumb",
	initialState: breadcrumbState,
	reducers: {
		setBreadcrumbList(state, { payload }) {
			state.breadcrumbList = payload;
		}
	}
});

export const { setBreadcrumbList } = breadcrumbSlice.actions;
export default breadcrumbSlice.reducer;
