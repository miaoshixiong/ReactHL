import { createSlice } from "@reduxjs/toolkit";

const menuState = {
	isCollapse: false,
	menuList: []
};

const menuSlice = createSlice({
	name: "menu",
	initialState: menuState,
	reducers: {
		updateCollapse(state, { payload }) {
			state.isCollapse = payload;
		},
		setMenuList(state, { payload }) {
			state.menuList = payload;
		}
	}
});

export default menuSlice.reducer;
export const { updateCollapse, setMenuList } = menuSlice.actions;
