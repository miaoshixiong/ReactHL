import { createSlice } from "@reduxjs/toolkit";

const authState = {
	authButtons: {},
	authRouter: []
};

const authSlice = createSlice({
	name: "auth",
	initialState: authState,
	reducers: {
		setAuthButtons(state, { payload }) {
			state.authButtons = payload;
		},
		setAuthRouter(state, { payload }) {
			state.authRouter = payload;
		}
	}
});

export const { setAuthButtons, setAuthRouter } = authSlice.actions;
export default authSlice.reducer;
