import { HOME_URL } from "@/config/config";
import { createSlice } from "@reduxjs/toolkit";

const tabsState = {
	// tabsActive 其实没啥用，使用 pathname 就可以了😂
	tabsActive: HOME_URL,
	tabsList: [{ title: "首页", path: HOME_URL }]
};

const tabsSlice = createSlice({
	name: "tabs",
	initialState: tabsState,
	reducers: {
		setTabsList(state, { payload }) {
			state.tabsList = payload;
		},
		setTabsActive(state, { payload }) {
			state.tabsActive = payload;
		}
	}
});

export const { setTabsList, setTabsActive } = tabsSlice.actions;
export default tabsSlice.reducer;
