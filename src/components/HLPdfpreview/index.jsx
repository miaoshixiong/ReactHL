import React, { useState } from 'react';
import { Document, Page , pdfjs } from 'react-pdf';
import './index.less'
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
function PDFApp({ prfUrl  }) {
  const [numPages, setNumPages] = useState(null);// 总页数
  const [pageNumber, setPageNumber] = useState(1);//页码
	const [pageNumberInput, setpageNumberInput] = useState(1);// input 显示的值
	const [pageNumberFocus, setpageNumberFocus] = useState(false);// 是否获得焦点
	const [pageWidth, setpageWidth] = useState(503);// 预览pdf的宽度
	const [fullscreen, setfullscreen] = useState(false);// 是否全屏
 
  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }
	
	const	lastPage = () => {
        if (pageNumber == 1) {
            return;
        }
        const page = pageNumber - 1;
				setPageNumber(page)
				setpageNumberInput(page)
    };
  const  nextPage = () => {
        if (pageNumber == numPages) {
            return;
        }
        const page = pageNumber + 1;
				setPageNumber(page)
				setpageNumberInput(page)
    };
  const  onPageNumberFocus = (e) => {
				setpageNumberFocus(true)
    };
  const  onPageNumberBlur = (e) => {
				setpageNumberFocus(false)
				setpageNumberInput(pageNumber)
    };
  const  onPageNumberChange = (e) => {
        let value = e.target.value;
        value = value <= 0 ? 1 : value;
        value = value >= numPages ? numPages : value;
				setpageNumberInput(value)
    };
  const  toPage = (e) => {
        if (e.keyCode === 13) {
						setPageNumber(Number(e.target.value))
        }
    };

  const  pageZoomOut = () => {
        if (pageWidth <= 503) {
            return;
        }
        const pageWidth2 = pageWidth * 0.8;
				setpageWidth(pageWidth2)
    };
  const  pageZoomIn = () => {
        const pageWidth2 = pageWidth * 1.2;
				setpageWidth(pageWidth2)
    };

  const  pageFullscreen = () => {
        if (fullscreen) {
					setfullscreen(false)
					setpageWidth(600)
        } else {
					setfullscreen(true)
					setpageWidth(window.screen.width - 40)
        }
    };

  return (
		<div className="pdf-view">
			<div className="container">
					<Document file={prfUrl} onLoadSuccess={onDocumentLoadSuccess} loading={'加载中...'} >
						<Page pageNumber={pageNumber} width={pageWidth} loading={'加载中...'} />
					</Document>
			</div>
			<div className="page-tool">
					<div className='page-tool-item' onClick={lastPage}> 上一页</div>
					<div className='page-tool-item' onClick={nextPage}> 下一页</div>
					<div className="input">
						<input 
						value={pageNumberFocus ? pageNumberInput : pageNumber}
						onFocus={onPageNumberFocus}
						onBlur={onPageNumberBlur}
						onChange={onPageNumberChange}
						onKeyDown={toPage}
						type="number" /> / {numPages}</div>
					<div className='page-tool-item'  onClick={pageZoomIn}> 放大</div>
					<div className='page-tool-item'  onClick={pageZoomOut}> 缩小</div>
					<div className="page-tool-item" onClick={pageFullscreen}>{fullscreen ? "恢复默认" : "适合窗口"}</div>
			</div>
			</div>
  );
}

export default PDFApp;
