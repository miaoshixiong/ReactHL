import { Switch } from "antd";
import { useSelector,useDispatch } from "@/redux";
import { setDark,setThemeConfig } from "@/redux/modules/global";

const SwitchDark = (props) => {
	const dispatch = useDispatch();
	// const { setThemeConfig, themeConfig } = props;
	const themeConfig = useSelector((state) => state.global.themeConfig);
	const onChange = (checked) => {
		// setThemeConfig({ ...themeConfig, isDark: checked });
		console.log(themeConfig)
		// dispatch(setDark(checked))
		dispatch(setThemeConfig({...themeConfig,isDark: checked}))
	};

	return (
		<Switch
			className="dark"
			defaultChecked={themeConfig.isDark}
			checkedChildren={<>🌞</>}
			unCheckedChildren={<>🌜</>}
			onChange={onChange}
		/>
	);
};

export default SwitchDark
