import React from "react";
import lazyLoad from "@/routers/utils/lazyLoad";
import { LayoutIndex } from "@/routers/constant";

// 常用组件模块
const assemblyRouter = [
	{
		element: <LayoutIndex />,
		meta: {
			title: "常用组件"
		},
		children: [
			{
				path: "/assembly/svgIcon",
				element: lazyLoad(React.lazy(() => import("@/pages/assembly/pdfview/index"))),
				meta: {
					requiresAuth: true,
					title: "PDF预览",
					key: "svgIcon"
				}
			},
			{
				path: "/assembly/selectIcon",
				element: lazyLoad(React.lazy(() => import("@/pages/assembly/selectIcon/index"))),
				meta: {
					requiresAuth: true,
					title: "批量导出数据",
					key: "selectIcon"
				}
			},
			{
				path: "/assembly/batchImport",
				element: lazyLoad(React.lazy(() => import("@/pages/assembly/batchImport/index"))),
				meta: {
					requiresAuth: true,
					title: "批量导入数据",
					key: "batchImport"
				}
			}
		]
	}
];

export default assemblyRouter;
