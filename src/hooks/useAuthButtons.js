import { searchRoute } from "@/utils/util";
import { useLocation } from "react-router-dom";
import { routerArray } from "@/routers";
import { useSelector } from "@/redux";

/**
 * @description 页面按钮权限 hooks
 * */
const useAuthButtons = () => {
	const { authButtons } = useSelector((state) => state.auth);
	const { pathname } = useLocation();
	const route = searchRoute(pathname, routerArray);
	console.log(authButtons)
	console.log(route)
	return {
		BUTTONS: authButtons[route.meta.key] || {}
	};
};

export default useAuthButtons;
