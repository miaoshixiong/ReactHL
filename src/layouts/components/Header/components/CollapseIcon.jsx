import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import {  useDispatch, useSelector } from "@/redux";
import { updateCollapse } from "@/redux/modules/menu";

const CollapseIcon = () => {
	const dispatch = useDispatch();
	const { isCollapse } = useSelector((state) => state.menu);

	return (
		<div
			className="collapsed"
			onClick={() => {
				dispatch(updateCollapse(!isCollapse));
			}}
		>
			{isCollapse ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
		</div>
	);
};

export default CollapseIcon;
